# CarMovement
# Moves the car and sees if the move is valid
import time 

def direction_choice(temp_x):
    directionLoop=True
    legal_direction=1
    while directionLoop==True:
        #User inputs movement direction
        direction=input("""\n\nWhich direction do you want to move the car or do you want to wait?
[Movement: w,a,s,d ; Wait: q] """).lower() 
        horizontal_movement=1
        vertical_movement=17
        no_movement=0
        directionLoop=False
        if direction=="a":
            temp_x-=horizontal_movement

        elif direction=="d":
            temp_x+=horizontal_movement

        elif direction=="w":
            temp_x-=vertical_movement
            
        elif direction=="s":
            temp_x+=vertical_movement

        elif direction=="q":
            temp_x+=no_movement
            
        else:
            legal_direction=0
            print("\nI N V A L I D    M O V E ")
            print("DIRECTION CANNOT BE RECOGNISED")
            time.sleep(1)
            
        return temp_x,legal_direction

    
def moveCar(x,gd,g1,g2,g3,g4,turnCounter):
    temp_x=x
    direction_choice_return=direction_choice(temp_x)
    temp_x=direction_choice_return[0]
    legal_move=True
    eventCheck=0
    inaccessible_terrain=['#','|' ,'-','*']

    if direction_choice_return[1]==0:
        legal_move=False

    if gd[temp_x] in inaccessible_terrain:

        print("\nI N V A L I D    M O V E ")

        if gd[temp_x] in ['#','|' ,'-']: 
            print("PLEASE STAY ON THE ROAD")
            time.sleep(1)

        elif gd[temp_x]=='*':
            print("YOU HAVE ALREADY BEEN TO THIS DESTINATION")
            time.sleep(1)
        legal_move=False

    if gd[temp_x]=='R':
        print("\nI N V A L I D    M O V E ")
        print("DO NOT DRIVE THROUGH A RED LIGHT, PLEASE WAIT BY ENTERING 'Q'")
        time.sleep(1)
        legal_move=False

    destinations=['X','Y','Z','W']
    if gd[temp_x] in destinations:  #Checks if you have reached the destination
        # Changes destinations that have already been travelled to into inaccessible terrain        
        if gd[temp_x]=='X':
            g1='*'
                  
        if gd[temp_x]=='Y':
            g2='*'
            
        if gd[temp_x]=='Z':
            g3='*'
 
        if gd[temp_x]=='W':
            g4='*'

        eventCheck=1

        legal_move=False
        turnCounter+=1
    
    if legal_move==True:
        turnCounter+=1
        legal_move_check=1
        return (temp_x,g1,g2,g3,g4,eventCheck,turnCounter,legal_move_check)

    else:
        legal_move_check=0
        return (x,g1,g2,g3,g4,eventCheck,turnCounter,legal_move_check)



        

        
