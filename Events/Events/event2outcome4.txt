You bring it home and start to read the manual. However it is hundreds of pages long and you fall asleep.
You and your friends start to play the game. However not knowing the rules causes you to get into a huge argument. You are now no longer friends.
You put the board game in with your Beznia collection. Along with your Sicky Ratto figuirines and Bimsa plushes.
When you go home and open it, you faint and wake up in a strange land filled with Beznia characters. It seems to have some narcotics stuck in.