Although a bit more expensive than you would have liked, the TV has many channels to keep you entertained and fuel your insomnia.
The TV does not have very many channels that you like at the time. Being unable to watch any horror does not help. You try to forget the image of the smiling clown laughing in the back of your head.
You go to sleep instantly after a long day of driving. Too bad, the extra money was useless.
The TV sadly does not turn on, and you complain to the staff. When they come in they see an off power switch...
