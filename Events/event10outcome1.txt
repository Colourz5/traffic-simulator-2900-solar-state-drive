You quickly drive home to retrieve your house keys. When you return home you see a thief taking all of your stuff. You shout "Hey! Stop!" but the thief doesn't care and punches you in the face.
You decide that it's too much effort to go back home to get your keys so you just keep on driving.
In panic you jump out of your car, fall on the ground, become unconscious and then wake up back in your car. It was all just a bad dream.
You go back home, climb in through your bathroom window and retrieve your house keys. You go back to driving and realise that in doing all that you have wasted thirty minutes of your life. 

 
