You furiously check all your pockets thinking that the car keys might be in one of them. They aren't. Life sucks. 
You panic thinking that you won't be able to get into your car but then you realise that you're already in the car.
You wonder how you're able to drive without car keys but then you shrug it off because some things in life aren't worth questioning. 
You sigh and wish that you weren't so forgetful. Well bad luck. We can't always get what we want.  