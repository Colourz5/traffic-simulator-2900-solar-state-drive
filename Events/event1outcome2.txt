You lose a lot of weight and have become a stick on the ground.
The skim milk skimmed off all your dead skin. You now glow in the dark.
Turns out you are allergic to milk without cream. You are now a ghost only able to drive.
The milk was fake and was just water with some calcium carbonate dissolved in it. But at least you got your daily calcium intake.