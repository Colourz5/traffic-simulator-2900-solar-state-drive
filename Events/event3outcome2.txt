You pick up the wallet and try to find the person who lost it. You spot a person looking frantically at the ground and give the wallet to them. They thank and leave.
You give it back to the person. They reward you with $400.
You fail to find them and decide to pass on the quest to another person.
The wallet has their ID so one day you go to their house to return it. When you arrive you knock on the door, but no one answers so you leave it on the ground.
