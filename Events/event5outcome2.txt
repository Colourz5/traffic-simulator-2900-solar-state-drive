You go to the amusement park by yourself. Using the regular customer coupon you spend hours going on all the rides many times over. What fun!
The tickets cost nearly a $1000. Being poor you question why you stopped at an amusement park in the first place...
The park was filled with children and it was hard to even move. So you only had time to ride your favourite ride once, Bhutan's Amazing World.
You enter Beznialand and find your favourite 90's character, Sicky Ratto and ask for a selfie. What a great time to be alive!