# Events
import random
import time

def event():
    eventloop=True
    while eventloop==True:
        eventnumber=random.randint(1,10)
        eventselect="event"+str(eventnumber)
        with open("Events/"+eventselect+".txt","r") as temp_e:
            scenario=[line.strip() for line in temp_e]
            temp_e.close
        print("\n") 
        print(scenario[0])
        with open("Events/"+eventselect+"choice.txt","r") as temp_e:
            eventchoice=[line.strip() for line in temp_e]
            temp_e.close
        print("\n1.",eventchoice[0])
        print("\n2.",eventchoice[1])
        print("\n3.",eventchoice[2])
        print("\n4.",eventchoice[3])
        try:
            eventpath=int(input("\nPlease choose an option between 1-4... "))
            EV_path=str(eventpath)
            if eventpath<5 and eventpath>0:
                with open("Events/"+eventselect+"outcome"+EV_path+".txt","r") as temp_e:
                    outcome=[line.strip() for line in temp_e]
                    temp_e.close
                ranOutcome=random.randint(0,3)
                time.sleep(0.5)
                print("")
                print("\nYou chose option",eventpath,"...",eventchoice[eventpath-1])
                print()
                time.sleep(1.5)
                print(outcome[ranOutcome])
            else:
                eventpath=str(random.randint(1,4))
                with open(eventselect+"outcome"+eventpath+".txt","r") as temp_e:
                    outcome=[line.strip() for line in temp_e]
                    temp_e.close
                ranOutcome=random.randint(0,3)
                print()
                time.sleep(1.5)
                print(outcome[ranOutcome])
        except:
            print("")
            print("\nThat was not a valid number so it was selected at random... ")
            eventpath=str(random.randint(1,4))
            with open("Events/"+eventselect+"outcome"+eventpath+".txt","r") as temp_e:
                outcome=[line.strip() for line in temp_e]
                temp_e.close
            ranOutcome=random.randint(0,3)
            print()
            time.sleep(1.5)
            print(outcome[ranOutcome])
        pointreward=random.randrange(-1100,1100,100)
        time.sleep(1)
        input("\nPoints earned: "+str(pointreward)+". [PRESS ENTER TO CONTINUE]")
        eventloop=False
    return pointreward

