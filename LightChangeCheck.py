#LightChangeCheck.py

import random 

def LightChangeCheck():
    change_light1=random.randint(0,1)
    change_light2=random.randint(0,1)
    change_light3=random.randint(0,1)
    change_light4=random.randint(0,1)
    change_light5=random.randint(0,1)
    return change_light1,change_light2,change_light3,change_light4,change_light5

def light1_change(light1):
    if light1=='G':
        light1='R'
    elif light1=='R':
        light1='G'
    return light1

def light2_change(light2):
    if light2=='G':
        light2='R'
    elif light2=='R':
        light2='G'
    return light2

def light3_change(light3):
    if light3=='G':
        light3='R'
    elif light3=='R':
        light3='G'
    return light3

def light4_change(light4):
    if light4=='G':
        light4='R'
    elif light4=='R':
        light4='G'
    return light4

def light5_change(light5):
    if light5=='G':
        light5='R'
    elif light5=='R':
        light5='G'
    return light5
