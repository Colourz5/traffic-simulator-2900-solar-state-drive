# The Mainline of Traffic Simulator 2900
import sys
from Events import events
import time
import random
import gameStart
import startmenu
import boards
import choosingBoard
import LightChangeCheck
import CarMovement
import scoreboard

#Removes car icon from tiles where the car is no longer located
def Update_Board():
    gd[y]=" "

#Prints the grid
def gridUpdate(turnCounter,objectives):
    global x
    global y
    gd[x]="C" 
    y=x
    print("\n\nT U R N  "+str(turnCounter)+"                DESTINATIONS REACHED: "+str(objectives))
    boards.gridDisplay(gd)
    print("P O I N T S: "+str(eventpoints)+"                      T R A F F I C")
    
#Intro
gameStart.opening_text()
print("\n") 

#Start Menu
startmenu.menu()

#Choosing the board
choice=choosingBoard.choosingBoard()

#Prepares the board based on choice
if choice==1:
    l1='G'
    l2='R'
    l3='R'
    l4='G'
    l5='G'
    g1='X'
    g2='Y'
    g3='Z'
    g4='W'
    def grid():
        grid=boards.board1(l1,l2,l3,l4,l5,g1,g2,g3,g4)
        return grid
    gd=grid() 
    starting_position=145    

elif choice==2:
    l1='G'
    l2='R'
    l3='R'
    l4='G'
    l5='G'
    g1='X'
    g2='Y'
    g3='Z'
    g4='W'
    def grid():
        grid=boards.board2(l1,l2,l3,l4,l5,g1,g2,g3,g4)
        return grid
    gd=grid()
    starting_position=159

elif choice==3:    
    l1='G'
    l2='R'
    l3='R'
    l4='G'
    l5='G'
    g1='X'
    g2='Y'
    g3='Z'
    g4='W'
    def grid():
        grid=boards.board3(l1,l2,l3,l4,l5,g1,g2,g3,g4)
        return grid
    gd=grid() 
    starting_position=217

# Defining variables
x=starting_position
y=''
turnCounter=1
objectives=0
eventpoints=0

# Printing tips
gameStart.tips()

# Starts game
gridUpdate(turnCounter,objectives)
Update_Board()
loop=True
eventCheck=0

while loop==True:
    # Moving the car
    movement_return_list=CarMovement.moveCar(x,gd,g1,g2,g3,g4,turnCounter)

    x=movement_return_list[0]

    g1=movement_return_list[1]

    g2=movement_return_list[2]

    g3=movement_return_list[3]

    g4=movement_return_list[4]

    eventCheck=movement_return_list[5]

    turnCounter=movement_return_list[6]

    legal_move_check=movement_return_list[7]

    time.sleep(0.25)

    #Prompts event to start
    if eventCheck==1:
        print("\n\nGood job, you reached the destination!")
        time.sleep(1.5)
        
        points=events.event() 

        objectives+=1

        eventpoints+=points 

        eventCheck=0

    #Traffic Lights Change Colours 
    if legal_move_check==1:
    
        changeChecker=LightChangeCheck.LightChangeCheck()

        if changeChecker[0]==1:
            l1=LightChangeCheck.light1_change(l1)

        if changeChecker[1]==1:
            l2=LightChangeCheck.light2_change(l2)

        if changeChecker[2]==1:
            l3=LightChangeCheck.light3_change(l3)

        if changeChecker[3]==1:
            l4=LightChangeCheck.light4_change(l4)

        if changeChecker[4]==1:
            l5=LightChangeCheck.light5_change(l5) 

    #Updates the board after user enters movement
    gd=grid()
    gridUpdate(turnCounter,objectives)
    Update_Board()

    if objectives==4:
        print("\nGood job, you have completed the game! ")
        time.sleep(0.5)
        loop=False

#Acquires scoreboard data        
name = input("\nPlease enter your name: ")

date = input("\nPlease enter the date [dd/mm/yyyy]: ")

scoreboard.scoreboard(name,eventpoints,turnCounter,date)

print("\nYour score has been saved! ")
time.sleep(1.5)
print("\nYou can check it in 'Scoreboard.txt'.")
time.sleep(1.5)
print("\nWe hope that you have enjoyed TRAFFIC SIMULATOR 2900! :D")
time.sleep(3)
input("\n\nPress enter to exit. ")
sys.exit() 



        

    
        
    
