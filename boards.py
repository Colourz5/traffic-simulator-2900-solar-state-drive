#Boards

def board1(l1,l2,l3,l4,l5,g1,g2,g3,g4):
    a="-"
    d="|"
    b='#'
    o=' '
    grid=[b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b,
          b, a, a, a, a, a, a, a, a, o, a, a, a, g1, a, a, b,
          b, d, o, o, o, o, o, o, d, o, d, o, o, o, o, d, b,
          b, d, o, a, a, a, a, o, d, o, d, o, a, a, o, d, b,
          b, d, o, d, o, o, d, o, a, a, a, o, d, d, o, d, b,
          b, d, o, g2, o, o, d, o, o, o, o, l1, d, d, o, d, b,
          b, d, o, d, o, o, d, o, d, a, d, o, d, d, o, d, b,
          b, d, o, a, a, a, a, o, d, a, d, o, d, d, o, d, b,
          b, d, o, l3, o, o, l2, o, o, o, o, l4, d, d, o, d, b,
          b, a, a, a, a, a, a, a, a, o, a, a, d, d, o, d, b,
          b, a, a, a, a, a, a, a, a, l5, a, a, a, a, o, d, b, 
          b, d, o, o, o, o, o, o, o, o, o, o, o, o, o, d, b,
          b, d, o, a, g3, a, a, a, a, a, a, a, a, a, o, d, b,
          b, d, o, a, a, a, a, a, a, a, a, o, o, d, o, d, b,
          b, d, o, o, o, o, o, o, o, o, d, o, o, a, g4, a, b,
          b, a, a, a, a, a, a, a, a, a, a, o, o, o, o, o, b,
          b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b]
    return grid
    
def board2(l1,l2,l3,l4,l5,g1,g2,g3,g4):
    a="-"
    d="|"
    b='#'
    o=' '
    grid=[b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b,
          b, o, a, g1, a, a, a, o, o, o, o, o, o, o, o, o, b,
          b, o, d, o, o, o, d, o, o, o, o, o, o, o, o, o, b,
          b, o, d, o, a, o, d, o, o, o, o, o, o, o, o, o, b,
          b, o, d, o, o, o, d, o, o, o, o, o, o, o, o, o, b,
          b, a, a, l1, a, o, a, a, a, a, o, a, a, a, g2, a, b,
          b, o, o, o, d, o, l2, o, o, d, o, d, o, o, o, d, b,
          b, o, a, o, d, o, a, a, o, g3, o, d, o, a, o, d, b,
          b, o, d, o, a, o, a, a, o, a, a, a, o, d, o, d, b,
          b, o, d, o, o, o, o, o, o, l3, o, l4, o, d, o, d, b,
          b, o, a, a, a, o, a, a, o, a, a, a, o, a, o, d, b, 
          b, o, o, d, d, o, d, d, o, d, o, d, o, o, o, d, b,
          b, a, o, a, a, o, a, a, o, d, o, d, o, a, a, a, b,
          b, d, o, o, o, l5, o, o, o, d, o, g4, o, d, o, o, b,
          b, a, a, a, a, o, a, a, o, a, a, a, o, d, o, o, b,
          b, o, o, o, d, o, o, o, o, o, o, o, o, d, o, o, b,
          b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b]
    return grid

def board3(l1,l2,l3,l4,l5,g1,g2,g3,g4):
    a="-"
    d="|"
    b='#'
    o=' '
    grid=[b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b,
          b, a, a, a, a, a, o, o, o, o, o, o, o, o, o, o, b,
          b, d, o, o, o, d, o, o, a, a, a, a, a, a, g1, a, b,
          b, d, o, d, o, g2, o, o, g3, o, o, o, o, o, o, d, b,
          b, d, o, d, o, d, o, o, d, o, d, a, a, d, l1, d, b,
          b, d, o, l2, o, d, a, a, a, o, d, a, a, d, o, d, b,
          b, d, o, a, a, a, d, o, o, o, o, o, o, o, o, d, b,
          b, d, o, d, o, o, d, o, a, a, a, a, a, o, a, a, b,
          b, d, o, d, o, o, d, o, d, o, o, o, d, o, o, o, b,
          b, d, o, a, a, g4, d, o, d, o, o, o, a, a, a, o, b,
          b, d, o, o, o, o, l4, o, d, o, o, o, o, o, d, o, b, 
          b, a, a, a, a, a, o, a, a, o, d, a, a, a, a, o, b,
          b, o, o, o, o, d, o, d, a, a, a, o, o, o, o, o, b,
          b, o, o, o, o, d, o, o, o, l3, o, o, d, a, d, o, b,
          b, o, o, o, o, a, a, a, a, a, a, o, d, a, d, o, b,
          b, o, o, o, o, o, o, o, o, o, d, o, o, o, o, o, b,
          b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b]
    return grid

def gridDisplay(gd): #Prints the grid
    print(gd[0]+"  "+gd[1]+"  "+gd[2]+"  "+gd[3]+"  "+gd[4]+"  "+gd[5]+"  "+gd[6]+"  "+gd[7]+"  "+gd[8]+"  "+gd[9]+"  "+gd[10]+"  "+gd[11]+"  "+gd[12]+"  "+gd[13]+"  "+gd[14]+"  "+gd[15]+"  "+gd[16])
    print(gd[17]+"  "+gd[18]+"  "+gd[19]+"  "+gd[20]+"  "+gd[21]+"  "+gd[22]+"  "+gd[23]+"  "+gd[24]+"  "+gd[25]+"  "+gd[26]+"  "+gd[27]+"  "+gd[28]+"  "+gd[29]+"  "+gd[30]+"  "+gd[31]+"  "+gd[32]+"  "+gd[33])
    print(gd[34]+"  "+gd[35]+"  "+gd[36]+"  "+gd[37]+"  "+gd[38]+"  "+gd[39]+"  "+gd[40]+"  "+gd[41]+"  "+gd[42]+"  "+gd[43]+"  "+gd[44]+"  "+gd[45]+"  "+gd[46]+"  "+gd[47]+"  "+gd[48]+"  "+gd[49]+"  "+gd[50])
    print(gd[51]+"  "+gd[52]+"  "+gd[53]+"  "+gd[54]+"  "+gd[55]+"  "+gd[56]+"  "+gd[57]+"  "+gd[58]+"  "+gd[59]+"  "+gd[60]+"  "+gd[61]+"  "+gd[62]+"  "+gd[63]+"  "+gd[64]+"  "+gd[65]+"  "+gd[66]+"  "+gd[67])
    print(gd[68]+"  "+gd[69]+"  "+gd[70]+"  "+gd[71]+"  "+gd[72]+"  "+gd[73]+"  "+gd[74]+"  "+gd[75]+"  "+gd[76]+"  "+gd[77]+"  "+gd[78]+"  "+gd[79]+"  "+gd[80]+"  "+gd[81]+"  "+gd[82]+"  "+gd[83]+"  "+gd[84])
    print(gd[85]+"  "+gd[86]+"  "+gd[87]+"  "+gd[88]+"  "+gd[89]+"  "+gd[90]+"  "+gd[91]+"  "+gd[92]+"  "+gd[93]+"  "+gd[94]+"  "+gd[95]+"  "+gd[96]+"  "+gd[97]+"  "+gd[98]+"  "+gd[99]+"  "+gd[100]+"  "+gd[101])
    print(gd[102]+"  "+gd[103]+"  "+gd[104]+"  "+gd[105]+"  "+gd[106]+"  "+gd[107]+"  "+gd[108]+"  "+gd[109]+"  "+gd[110]+"  "+gd[111]+"  "+gd[112]+"  "+gd[113]+"  "+gd[114]+"  "+gd[115]+"  "+gd[116]+"  "+gd[117]+"  "+gd[118])
    print(gd[119]+"  "+gd[120]+"  "+gd[121]+"  "+gd[122]+"  "+gd[123]+"  "+gd[124]+"  "+gd[125]+"  "+gd[126]+"  "+gd[127]+"  "+gd[128]+"  "+gd[129]+"  "+gd[130]+"  "+gd[131]+"  "+gd[132]+"  "+gd[133]+"  "+gd[134]+"  "+gd[135])
    print(gd[136]+"  "+gd[137]+"  "+gd[138]+"  "+gd[139]+"  "+gd[140]+"  "+gd[141]+"  "+gd[142]+"  "+gd[143]+"  "+gd[144]+"  "+gd[145]+"  "+gd[146]+"  "+gd[147]+"  "+gd[148]+"  "+gd[149]+"  "+gd[150]+"  "+gd[151]+"  "+gd[152])
    print(gd[153]+"  "+gd[154]+"  "+gd[155]+"  "+gd[156]+"  "+gd[157]+"  "+gd[158]+"  "+gd[159]+"  "+gd[160]+"  "+gd[161]+"  "+gd[162]+"  "+gd[163]+"  "+gd[164]+"  "+gd[165]+"  "+gd[166]+"  "+gd[167]+"  "+gd[168]+"  "+gd[169])
    print(gd[170]+"  "+gd[171]+"  "+gd[172]+"  "+gd[173]+"  "+gd[174]+"  "+gd[175]+"  "+gd[176]+"  "+gd[177]+"  "+gd[178]+"  "+gd[179]+"  "+gd[180]+"  "+gd[181]+"  "+gd[182]+"  "+gd[183]+"  "+gd[184]+"  "+gd[185]+"  "+gd[186])
    print(gd[187]+"  "+gd[188]+"  "+gd[189]+"  "+gd[190]+"  "+gd[191]+"  "+gd[192]+"  "+gd[193]+"  "+gd[194]+"  "+gd[195]+"  "+gd[196]+"  "+gd[197]+"  "+gd[198]+"  "+gd[199]+"  "+gd[200]+"  "+gd[201]+"  "+gd[202]+"  "+gd[203])
    print(gd[204]+"  "+gd[205]+"  "+gd[206]+"  "+gd[207]+"  "+gd[208]+"  "+gd[209]+"  "+gd[210]+"  "+gd[211]+"  "+gd[212]+"  "+gd[213]+"  "+gd[214]+"  "+gd[215]+"  "+gd[216]+"  "+gd[217]+"  "+gd[218]+"  "+gd[219]+"  "+gd[220])
    print(gd[221]+"  "+gd[222]+"  "+gd[223]+"  "+gd[224]+"  "+gd[225]+"  "+gd[226]+"  "+gd[227]+"  "+gd[228]+"  "+gd[229]+"  "+gd[230]+"  "+gd[231]+"  "+gd[232]+"  "+gd[233]+"  "+gd[234]+"  "+gd[235]+"  "+gd[236]+"  "+gd[237])
    print(gd[238]+"  "+gd[239]+"  "+gd[240]+"  "+gd[241]+"  "+gd[242]+"  "+gd[243]+"  "+gd[244]+"  "+gd[245]+"  "+gd[246]+"  "+gd[247]+"  "+gd[248]+"  "+gd[249]+"  "+gd[250]+"  "+gd[251]+"  "+gd[252]+"  "+gd[253]+"  "+gd[254])
    print(gd[255]+"  "+gd[256]+"  "+gd[257]+"  "+gd[258]+"  "+gd[259]+"  "+gd[260]+"  "+gd[261]+"  "+gd[262]+"  "+gd[263]+"  "+gd[264]+"  "+gd[265]+"  "+gd[266]+"  "+gd[267]+"  "+gd[268]+"  "+gd[269]+"  "+gd[270]+"  "+gd[271])
    print(gd[272]+"  "+gd[273]+"  "+gd[274]+"  "+gd[275]+"  "+gd[276]+"  "+gd[277]+"  "+gd[278]+"  "+gd[279]+"  "+gd[280]+"  "+gd[281]+"  "+gd[282]+"  "+gd[283]+"  "+gd[284]+"  "+gd[285]+"  "+gd[286]+"  "+gd[287]+"  "+gd[288])


