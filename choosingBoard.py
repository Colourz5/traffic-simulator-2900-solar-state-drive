#Choosing Board
import time
def choosingBoard():
    choiceLoop=True
    while choiceLoop==True:
        try:
            boardChoice=int(input("""\nWhich board would you like to play on?

|---|         |---|         |---|
| 1 |         | 2 |         | 3 |       
|---|         |---|         |---|

Please enter the number which corresponds to your choice: """))
            intCheck=True
        except:
            input("\nPlease only enter an integer value between 1 and 3 (inclusive). [Press enter to continue] ")
            time.sleep(1)
            intCheck=False
        if intCheck==True:
            if boardChoice not in [1,2,3]:
                input("\nPlease only enter an integer value between 1 and 3 (inclusive). [Press enter to continue] ")
            else:
                choiceLoop=False

    print("\nYou have chosen Board "+str(boardChoice)+". ")
    time.sleep(1) 
    return boardChoice
