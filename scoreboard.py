# scoreboard

def scoreboard(name,eventpoints,turnstaken,date):
    points = (eventpoints*3)-(2*turnstaken)
    evpo = str(eventpoints)
    tuta = str(turnstaken)
    po = str(points)

    with open("Scoreboard.txt","a") as file:
        file.write("\n")
        file.write("Player Name: ")
        file.write(name)
        file.write("\n")
        file.write("Date Played: ")
        file.write(date)
        file.write("\n")
        file.write("Event points obtained: ")
        file.write(evpo)
        file.write("\n")
        file.write("Turns Taken: ")
        file.write(tuta)
        file.write("\n")
        file.write("Total Points Earned: ")
        file.write(po)
        file.write("\n")
    file.close


